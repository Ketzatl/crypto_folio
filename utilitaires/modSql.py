import sqlite3

from utilitaires.modFunctions import mydatetime,crypto_list,cours_liste_totale



bdd = 'databases/coursCrypto.db'
connecteur = sqlite3.connect(bdd)
curseur = connecteur.cursor()
dateToday = str(mydatetime)
table = "TableCours"



def createTable():
    """
    Fonction qui créé une table dans la base de données "bdd"

    """
    curseur.execute(
        f"CREATE TABLE IF NOT EXISTS {table} (id integer primary key autoincrement, Token char(13), Cours char(13) )")
    connecteur.commit()
    print(' -------- La table a bien été créée et sauvegardée ! --------')



def insertInto():
    """
    Fonction qui insert dans la table sqlite3, les données de cours_crypto()

    """
    temp_count = 0
    crypto_list[temp_count]
    for token in crypto_list:
        params = (temp_count, crypto_list[temp_count], cours_liste_totale[temp_count])
        curseur.execute(
            f"INSERT OR IGNORE INTO {table} VALUES (?, ?, ?)", params)
        connecteur.commit()
        temp_count += 1
    print("\n")
    print(
        f'        -------- Les Valeurs ont bien été insérées dans la table {table} de la Base de Données : {bdd} ! --------')
    print("\n")


"""def selectAll():
    a = pd.read_sql_query(f'SELECT * FROM {sqlClass.table};', con=sqlClass.connecteur)
    print(a)"""