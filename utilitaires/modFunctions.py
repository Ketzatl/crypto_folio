import requests
from datetime import datetime

from utilitaires.colorsClass import Colors




BASE_URL = "https://min-api.cryptocompare.com/data/price?fsym="
API_KEY = ""
SECRET_KEY = ""

crypto_list = ['BTC', 'ETH', 'DOT', 'CRO', 'LINK', 'LTC', 'EGLD', 'AAVE', 'ZIL', 'SNX', 'THETA', 'CHZ']
nb_crypto = len(crypto_list)
content = None
cours_list = []
cours_liste_totale = []
path = ""
file_name = ""

mydatetime = datetime.today()
mydatetime = mydatetime.strftime('%Y-%m-%d_%H:%M')


def cours_crypto(TOKEN_1):
    """
    Cette méthode envoie une requête à l'API afin d'obtenir le cours des crypto-monnaies et tokens listés
    dans crypto_list, dans la monnaie FIAT placée en paramètre (TOKEN_1) - exemples : EUR USD etc...

    :param TOKEN_1: (string)
    par defaut TOKEN_1 = 'EUR' (euros) mais il est possible de changer avec la monnaie FIAT voulue
    :return: (list)    Crypto.cours_list
    (ex : [{'EUR': 31147.04}, {'EUR': 1350.18}, {'EUR': 16.78},...]

    """
    print(f"\n {Colors.BOLD}{Colors.GREEN}Date : {mydatetime}{Colors.BOLD}{Colors.ENDC} \n")
    for i in crypto_list:
        response = requests.get(
            f'{BASE_URL}{i}&tsyms={TOKEN_1}')
        content = response.json()
        cours_list.append(content)
        for k, v in content.items():
            cours_liste_totale.append(v)

        print(f"{Colors.BOLD}{Colors.YELLOW}Cours du {i} :{Colors.PINK} {content}{Colors.ENDC}{Colors.BOLD}")
    print("\n")
    #print(Crypto.cours_liste_totale)
    #print(Crypto.content)



def insertInFile(list):
    """
    Cette méthode affiche dans la console la liste des cours des crypto-monnaies et tokens listés et l'heure
    de la requête, et créé un fichier .txt dans lequel sera inséré cette liste.
    :param list: Crypto.cours_
    (ex : [{'EUR': 31147.04}, {'EUR': 1350.18}, {'EUR': 16.78},...]
    :return: ----
    """
    crypto_count = 0
    path = "out/files/"
    file_name = mydatetime + "_" + 'coursCrypto.txt'
    with open(path + file_name, 'w') as file:
        file.write(f"Date : {mydatetime} \n")
        for i in list:
            file.write(f"Cours du {crypto_list[crypto_count]} : {i} \n")
            crypto_count += 1
    print(f'Le fichier {Colors.BOLD}{Colors.GREEN}{file_name}{Colors.BOLD}{Colors.ENDC} a bien été créé')
    print(f"{Colors.BOLD}{Colors.BLUE}{crypto_count}/{nb_crypto} entités{Colors.BOLD}{Colors.ENDC} insérés dans le fichier {Colors.BOLD}{Colors.BLUE}{file_name}{Colors.BOLD}{Colors.ENDC}")
    print(f'{Colors.BOLD}{Colors.RED}Fin de processus{Colors.BOLD}{Colors.ENDC} \n')



